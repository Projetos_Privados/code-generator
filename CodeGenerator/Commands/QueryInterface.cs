﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;
using CodeGenerator.Generators.Domain.Interfaces;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("query-interface", "qi")]
    public class QueryInterface : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity for query")]
        public string Name { get; set; }

        public void OnExecute()
        {
            new QueryInterfaceGenerator(
                    Name.ToFirstUpper()          
                ).SaveToFile(Force);
        }
    }
}
