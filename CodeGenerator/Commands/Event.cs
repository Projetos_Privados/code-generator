﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("event", "ev")]
    public class Event : CommandBase
    {
        [Required]
        [Argument(0, "Name of the event")]
        public string Name { get; set; }

        [Option( Description = "name:type:[y|n] | Simple property to add in model")]
        public IEnumerable<string> Properties { get; set; }

        public void OnExecute()
        {
            new EventGenerator(
                    Name.ToFirstUpper(), 
                    Properties.ToProperties()            
                ).SaveToFile(Force);
        }
    }
}
