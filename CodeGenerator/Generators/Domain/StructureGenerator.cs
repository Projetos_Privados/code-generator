﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CodeGenerator.Generators.Domain
{
    public class StructureGenerator
    {

        private List<string> Folders { get; set; } = new List<string> {
            "AggregateModels",
            "Commands",
            "CommandHandlers",
            "Constants",
            "Event",
            "EventHandlers",
            "Interfaces",
            "Interfaces/Queries",
            "Interfaces/Repositories",
            "Queries",
            "Queries/Specifications",
            "Validations",
            "Validations/Commands",
            "ValueObjects"
            };

        public StructureGenerator()
        {
            foreach(var folder in Folders)
            {
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);
            }
        }
    }
}
