﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("domain", "d")]
    public class DomainStructure : CommandBase
    {
        public void OnExecute()
        {
            new StructureGenerator( );
        }
    }
}
