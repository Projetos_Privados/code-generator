﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Infrastructure;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("repository", "repo", "r")]
    public class Repository : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity")]
        public string Name { get; set; }

        [Argument(1, "Name of the context")]
        public string ContextName { get; set; }

        public void OnExecute()
        {
            if (String.IsNullOrEmpty(ContextName))
                ContextName = Util.GetDomain();

            new RepositoryGenerator(
                    Name,
                    ContextName
                ).SaveToFile(Force);
        }
    }
}
