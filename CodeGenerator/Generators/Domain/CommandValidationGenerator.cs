﻿using System.Collections.Generic;

namespace CodeGenerator.Generators.Domain
{
    public class CommandValidationGenerator : Generator
    {
        public CommandValidationGenerator(string commandName, string entity, IEnumerable<Property> queries) : base($"{commandName}CommandValidation")
        {
            _builder
                .AddUsings( "FluentValidation" )
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{commandName}CommandValidation", new List<Modifier> { Modifier.PUBLIC }, $"CommandHandlerValidation<{commandName}Command, {entity}>")
                ;

            foreach (var prop in queries)
            {
                prop.SetAsQuery();
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY }, Modifier.NONE, Modifier.NONE);
            }

            _builder
                .AddConstructor(true)
                .CloseAllBrackets();
        }
    }
}
