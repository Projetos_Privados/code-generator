﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeGenerator.Extension
{
    public static class IEnumerableExtension
    {
        public static IEnumerable<Property> ToProperties(this IEnumerable<string> list, char separator = ':')
        {
            if (list == null)
                return new List<Property>();

            return list.Select(x =>
            {
                var values = x.Split(separator);
                var init = true;
                if(values.Count() > 2)
                {
                    if (values[2].ToLower() == "n")
                        init = false;
                }

                return new Property(values[0], values.Count() == 1 ? values[0] : values[1], init );
            });
        }
    }
}
