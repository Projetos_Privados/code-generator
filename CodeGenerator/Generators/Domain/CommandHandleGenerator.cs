﻿using CodeGenerator.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeGenerator.Generators.Domain
{
    public class CommandHandleGenerator : Generator
    {
        private IEnumerable<Property> BaseProperties { get; set; }

        public CommandHandleGenerator(string commandName, string entity, List<Property> repositories, List<Property> queries) : base($"{commandName}CommandHandler")
        {
            if (repositories == null)
                repositories = new List<Property>();

            if (queries == null)
                queries = new List<Property>();

            if (BaseProperties == null)
                BaseProperties = new List<Property>();

            _builder
                .AddUsings("MediatR")
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{commandName}CommandHandler", new List<Modifier> { Modifier.PUBLIC }, $"CommandHandler<{commandName}Command, {entity}>");

            for (int i = 0; i < repositories.Count; i++)
            {
                repositories[i].SetAsRepository();
                _builder.AddProperty(repositories[i], new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY });
            }

            for (int i = 0; i < queries.Count(); i++)
            {
                queries[i].SetAsQuery();
                _builder.AddProperty(queries[i], new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY });
            }

            BaseProperties = new List<Property>
            {
                new Property("bus", "IMediatorHandler", false, true),
                new Property("notificationHandler", "INotificationHandler<DomainNotification>", false, true),
                new Property($"new {commandName.ToFirstUpper()}CommandValidation({String.Join(", ", queries.Select( x => x.GetAsParam()).ToArray())})", "INotificationHandler<DomainNotification>", false, true),
            };

            _builder
                .AddConstructor(true, true, BaseProperties)
                .AddMethod("Handle", $"Task<{entity}>",
                    new List<Modifier> {
                        Modifier.PUBLIC, Modifier.ASYNC
                    },
                    new List<Property> {
                        new Property("command", $"{commandName}Command"),
                        new Property("cancellationToken", "CancellationToken")
                    },
                    new List<string> {
                        "var valid = await IsValidAsync(command, cancellationToken);",
                        "if(!valid)",
                        "return null;"
                    })
                .CloseAllBrackets()
                ;
        }
    }
}
