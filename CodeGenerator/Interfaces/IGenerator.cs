﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Interfaces
{
    public interface IGenerator
    {
        string Generate( );
    }
}
