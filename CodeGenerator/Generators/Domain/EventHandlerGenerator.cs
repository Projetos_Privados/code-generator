﻿using System.Collections.Generic;

namespace CodeGenerator.Generators.Domain
{
    public class EventHandlerGenerator : Generator
    {
        private IEnumerable<Property> BaseProperties { get; set; }

        public EventHandlerGenerator(string eventName, IEnumerable<Property> repositories, IEnumerable<Property> queries) : base($"{eventName}EventHandler")
        {
            if (repositories == null)
                repositories = new List<Property>();

            if (queries == null)
                queries = new List<Property>();

            if (BaseProperties == null)
                BaseProperties = new List<Property>();

            _builder
                .AddUsings("MediatR" )
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{eventName}EventHandler", new List<Modifier> { Modifier.PUBLIC }, $"INotificationHandler<{eventName}Event>")
                .AddProperty(new Property("eventStore", "IEventStore"), new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY })
                ;

            foreach (var prop in repositories)
            {
                prop.SetAsRepository();
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY });
            }

            foreach (var prop in queries)
            {
                prop.SetAsQuery();
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY });
            }

            _builder
                .AddConstructor(true)
                .AddMethod("Handle", $"Task",
                    new List<Modifier> {
                        Modifier.PUBLIC, Modifier.ASYNC
                    },
                    new List<Property> {
                        new Property("notification", $"{eventName}Event"),
                        new Property("cancellationToken", "CancellationToken")
                    },
                    new List<string> {
                        "await _eventStore.MarkEventAsProcessedAsync(notification.Id, cancellationToken);"
                    })
                .CloseAllBrackets()
                ;
        }
    }
}
