﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("query", "q")]
    public class Query : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity for query")]
        public string Name { get; set; }

        [Option( Description = "name:[type]:[y|n] | Repositories to add in model")]
        public IEnumerable<string> Repositories { get; set; }

        public void OnExecute()
        {
            new QueryGenerator(
                    Name.ToFirstUpper(), 
                    Repositories.ToProperties().ToList()            
                ).SaveToFile(Force);
        }
    }
}
