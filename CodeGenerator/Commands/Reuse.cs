﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Presentation;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands {
    [HelpOption( )]
    [Command( "reuse" )]
    public class Reuse: CommandBase {
        [Required]
        [Argument( 0, "Path of source files" )]
        public string Source { get; set; }

        [Required]
        [Argument( 1, "Path of destine files" )]
        public string Dest { get; set; }

        [Required]
        [Argument( 2, "Old namespace" )]
        public string OldNamespace { get; set; }

        [Required]
        [Argument( 3, "New namespace" )]
        public string NewNamespace { get; set; }

        public void OnExecute( ) {
            var files = Directory.GetFiles( Source, "*.cs", SearchOption.AllDirectories );
            foreach ( var file in files ) {
                var text = File.ReadAllText( file ).Replace( OldNamespace, NewNamespace );
                var path = Path.GetFullPath( file ).Replace( Source, Dest );
                if ( !Directory.Exists( Path.GetDirectoryName( path ) ) )
                    Directory.CreateDirectory( Path.GetDirectoryName( path ) );
                File.WriteAllText( $"{path}", text );
            }
        }
    }
}
