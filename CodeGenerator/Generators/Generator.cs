﻿using CodeGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CodeGenerator.Generators
{
    public class Generator : IGenerator
    {
        protected StringBuilder _builder;

        protected string Filename { get; set; }

        public Generator(string filename)
        {
            Filename = filename;

            _builder = new StringBuilder();
        }
        public string Generate()
        {
            return _builder.Build();
        }

        public void SaveToFile(bool force = false)
        {
            if (File.Exists($"{Filename}.cs") && !force)
                throw new Exception("File already exists. Use -f|--force to override.");            
            else            
                File.WriteAllText($"{Filename}.cs", Generate());
            
        }
    }
}
