﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Presentation
{
    public class AutoMapperGenerator : Generator
    {
        public AutoMapperGenerator(string sourceName, string destName) : base($"{sourceName}Profile")
        {
            _builder
                .AddUsings("AutoMapper")
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{sourceName}Profile", new List<Modifier> { Modifier.PUBLIC }, "Profile")
                .AddConstructor(bodyLines: new List<string> { $"CreateMap<{sourceName}, {destName}>();" })
                .CloseAllBrackets();
        }
    }
}
