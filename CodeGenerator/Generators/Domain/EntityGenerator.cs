﻿using CodeGenerator.Generators;
using System.Collections.Generic;
using System.Linq;

namespace CodeGenerator
{
    public class EntityGenerator : Generator
    {
        public EntityGenerator(string className,
            IEnumerable<Property> simpleProperties,
            IEnumerable<Property> customProperties)
            : base(className)
        {
            if (simpleProperties == null)
                simpleProperties = new List<Property>();

            if (customProperties == null)
                customProperties = new List<Property>();

            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass(className, new List<Modifier> { Modifier.PUBLIC });

            foreach (var prop in simpleProperties)
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PUBLIC }, setModifier: Modifier.PRIVATE);

            foreach (var prop in customProperties)
                _builder.AddPropertyAsReadOnly(prop, new List<Modifier> { Modifier.PUBLIC, Modifier.VIRTUAL });

            if (simpleProperties.Any(x => x.Initialize))
                _builder
                    .AddConstructor();

            _builder
            .AddConstructor(true, false)
            .CloseAllBrackets();
        }
    }
}
