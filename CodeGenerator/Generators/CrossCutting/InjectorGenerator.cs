﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.CrossCutting
{
    public class InjectorGenerator : Generator
    {
        public InjectorGenerator() : base($"InjectorContainer")
        {
            _builder
                .AddUsings("Microsoft.Extensions.DependencyInjection", "MediatR" )
                .AddNamespace(Util.GetNamespace())
                .AddClass("InjectorContainer", new List<Modifier> { Modifier.PUBLIC, Modifier.STATIC })
                .AddMethod("Register", "void", new List<Modifier> { Modifier.PUBLIC, Modifier.STATIC }, new List<Property> { new Property("services", "IServiceCollection"), new Property("connectionString", "string") },
                    new List<string> {
                        "#region [ Context ]", "", "","", "#endregion [ Context ]", "",
                        "#region [ Repository ]", "", "","", "#endregion [ Repository ]", "",
                        "#region [ Query ]", "", "","", "#endregion [ Query ]", "",
                        "#region [ Command ]", "", "","", "#endregion [ Command ]", "",
                        "#region [ Event ]", "", "","", "#endregion [ Event ]",
                    })
                .CloseAllBrackets();
        }
    }
}
