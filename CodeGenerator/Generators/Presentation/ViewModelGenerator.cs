﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Presentation
{
    public class ViewModelGenerator : Generator
    {
        public ViewModelGenerator(string viewModelName, List<Property> properties) : base($"{viewModelName}ViewModel")
        {
            if (properties == null)
                properties = new List<Property>();

            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{viewModelName}ViewModel", new List<Modifier> { Modifier.PUBLIC });

            foreach (var prop in properties)
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PUBLIC });

            _builder
                .AddConstructor(true)
                .CloseAllBrackets();
        }
    }
}
