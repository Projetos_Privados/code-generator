﻿namespace CodeGenerator
{
    public enum Modifier { PUBLIC, PRIVATE, PROTECTED, STATIC, OVERRIDE, ASYNC, VIRTUAL, NONE, READONLY }
}
