﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("command-validation", "cv")]
    public class CommandValidation : CommandBase
    {
        [Required]
        [Argument(0, "Name of the command to validate")]
        public string CommandName { get; set; }

        [Required]
        [Argument(1, "The entity type to return")]
        public string Entity { get; set; }

        [Option(Description = "name | Lists property to add in model")]
        public IEnumerable<string> Queries { get; set; }

        public void OnExecute()
        {
            new CommandValidationGenerator(
                    CommandName.ToFirstUpper(), 
                    Entity,
                    Queries.ToProperties()               
                ).SaveToFile(Force);
        }
    }
}
