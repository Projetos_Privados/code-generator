﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CodeGenerator
{
    public class Util
    {
        public static string GetDomain()
        {
            var directory = Directory.GetCurrentDirectory();

            var folders = directory.Split(@"\");
            var folderLevels = folders.Length;

            while (folderLevels <= folders.Length)
            {
                if (CheckIfContainsProject(directory))
                    break;
                else
                {
                    directory = Path.GetFullPath(Path.Combine(directory, @"..\"));
                    folderLevels--;
                    if (folderLevels > folders.Length)
                        throw new Exception("Can't find the current namespace");
                }
            }

            folderLevels--;
            var space = folders[folderLevels];
            var domain = space.Split('.')[0];
            return domain;
            
        }

        public static string GetNamespace()
        {
            var directory = Directory.GetCurrentDirectory();

            var folders = directory.Split(@"\");
            var folderLevels = folders.Length;

            while (folderLevels <= folders.Length)
            {
                if (CheckIfContainsProject(directory))
                    break;
                else
                {
                    directory = Path.GetFullPath(Path.Combine(directory, @"..\"));
                    folderLevels--;
                    if (folderLevels > folders.Length)
                        throw new Exception("Can't find the current namespace");
                }
            }

            var nameSpace = "";
            folderLevels--;
            for (int i = folderLevels; i < folders.Length; i++)
            {
                nameSpace = nameSpace + "." + folders[i];
            }
            nameSpace = nameSpace.Remove(0, 1);
            return nameSpace;
        }

        private static bool CheckIfContainsProject(string directory)
        {
            if (Directory.GetFiles(directory, "*.csproj").Length == 0)
                return false;
            else
                return true;
        }
    }
}
