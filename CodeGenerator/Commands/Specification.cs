﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("specification", "spec", "s")]
    public class Specification : CommandBase
    {
        [Required]
        [Argument(0, "Name of entity")]
        public string Entity { get; set; }

        public void OnExecute()
        {
            new SpecificationGenerator(
                    Entity,
                    Entity
                ).SaveToFile(Force);
        }
    }
}
