﻿using CodeGenerator.Extension;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeGenerator
{

    public static partial class StringBuilderExtension {
        private static string ClassName { get; set; }
        private static int BracketsToClose { get; set; }
        private static List<Property> Properties { get; set; } = new List<Property>( );
        private static Queue<string> Regions { get; set; } = new Queue<string>();

        public static StringBuilder AddUsings( this StringBuilder builder, params string[] usings ) {
            builder.AppendLine( "using System;" );
            builder.AppendLine( "using System.Linq;" );
            builder.AppendLine( "using System.Collections.Generic;" );
            if(usings != null)
                foreach (var item in usings)
                    builder.AppendLine( $"using {item};" );
            builder.AppendLine( );
            return builder;
        }

        public static StringBuilder AddNamespace( this StringBuilder builder, string namespaceName ) {
            builder.AppendLine( $"namespace { namespaceName }" );
            builder.AddBracket( );
            builder.AppendLine( );
            return builder;
        }

        public static StringBuilder AddClass( this StringBuilder builder, string className, IEnumerable<Modifier> modifiers = null, string extendsClass = null, IEnumerable<string> implementsClass = null, IEnumerable<AttributeParam> attributes = null ) {

            className = className.ToFirstUpper();

            if (attributes != null)
            {
                foreach( var attr in attributes)
                {
                    builder.Append("\t");
                    builder.AppendLine(attr.ToString());
                }
            }

            builder.Append("\t");
            builder.AddModifier( modifiers );

            builder.Append( $"class { className }" );

            if(extendsClass != null || implementsClass != null) {
                builder.Append(" : ");
                if(extendsClass != null) {
                    builder.Append(extendsClass);
                    if(implementsClass != null)
                        builder.Append(", ");
                }
                if(implementsClass != null)
                {
                    foreach(var imp in implementsClass)
                    {
                        builder.Append(imp);
                        if(imp != implementsClass.Last())
                        {
                            builder.Append(", ");
                        }
                    }
                }                
            }

            builder.AddBracket( );
            builder.AppendLine( );

            ClassName = className;

            return builder;
        }

        public static StringBuilder AddProperty( this StringBuilder builder, Property property, IEnumerable<Modifier> modifiers = null, Modifier getModifier = Modifier.PUBLIC, Modifier setModifier = Modifier.PUBLIC, string defaultValue = null ) {
            builder.Append("\t\t");
            builder.AddModifier( modifiers );
            string gMod = getModifier != Modifier.PUBLIC ? ModifierToString( getModifier ) : "";
            string sMod = setModifier != Modifier.PUBLIC ? ModifierToString( setModifier ) : "";

            builder.Append($"{property.Type} {property.Name} ");

            if (getModifier != Modifier.NONE || setModifier != Modifier.NONE)
                builder.Append("{ ");

            if (getModifier != Modifier.NONE)
                builder.Append($" {gMod} get; " );

            if (getModifier != Modifier.NONE)
                builder.Append($"{sMod} set; ");

            if (getModifier != Modifier.NONE || setModifier != Modifier.NONE)
                builder.Append("}");

            if ( defaultValue != null )
                builder.Append( $" = {defaultValue}" );

            if (getModifier == Modifier.NONE && setModifier == Modifier.NONE)
                builder.Append(";");

            builder.AppendLine();

            Properties.Add( property );

            return builder;
        }

        public static StringBuilder AddPropertyAsReadOnly(this StringBuilder builder, Property property, IEnumerable<Modifier> modifiers = null)
        {
            builder.Append("\t\t");
            builder.AddProperty(new Property($"_{property.Name.ToCamelCase()}", $"List<{property.Type}>"), new List<Modifier> { Modifier.PRIVATE }, Modifier.NONE, Modifier.NONE, $"new List<{property.Type}>()");

            builder.AddModifier(modifiers);
            builder.Append($"IEnumerable<{property.Type}> {property.Name} => _{property.Name.ToCamelCase()}.AsReadOnly()");
            
            return builder.AppendLine(";");
        }

        public static StringBuilder AddConstructor( this StringBuilder builder, bool initializeProperties = false, bool hasBase = false, IEnumerable<Property> otherParams = null, IEnumerable<string> bodyLines = null ) {
            builder.AppendLine();
            builder.Append("\t\t");
            builder.Append( $"public {ClassName} ( " );

            if (otherParams != null)            
                Properties.AddRange(otherParams);

            if (initializeProperties)
            {
                var param = Properties.Where(x => x.Initialize).Concat(Properties.Where(x => x.ForBase && !x.Initialize && !x.Name.ToLower().Contains("validation")));
                foreach (var prop in param)
                {
                    builder.Append($"{prop.Type} {prop.GetAsParam()}");
                    if (prop != param.Last())
                        builder.Append(", ");
                }
            }

            builder.AppendLine( " )" );

            if (hasBase)
            {
                builder.Append(": base ( ");
                foreach (var prop in Properties.Where( x => x.ForBase))
                {
                    if(!prop.Name.ToLower().Contains("validation"))
                        builder.Append($"{ prop.Name.ToCamelCase()}");
                    else
                        builder.Append($"{ prop.Name}");

                    if (prop != Properties.Where(x => x.ForBase).Last())
                        builder.Append(", ");
                }
                builder.Append(" )");
            }

            builder.Append("\t\t");
            builder.AddBracket( );

            if ( initializeProperties ) {
                foreach ( var prop in Properties.Where(x => x.Initialize && !x.ForBase)) {
                    builder.AppendLine( $"{prop.Name} = {prop.GetAsParam()};" );
                }
            }

            if(bodyLines != null)
            {
                foreach(var line in bodyLines)
                {
                    builder.AppendLine(line);
                }
            }

            builder.Append("\t\t");
            builder.CloseBracket( );
            builder.AppendLine( );
            return builder;
        }

        public static StringBuilder AddMethod( this StringBuilder builder, string methodName, string returnType, IEnumerable<Modifier> modifiers, IEnumerable<Property> parameters = null, IEnumerable<string> bodyLine = null, IEnumerable<AttributeParam> attributes = null)
        {
            builder.Append("\t\t");
            if (attributes != null)
            {
                foreach (var attr in attributes)
                {
                    builder.AppendLine(attr.ToString());
                }
            }

            builder.AddModifier(modifiers);

            builder.Append( $"{returnType} {methodName} ( " );
            if (parameters != null)
            {
                foreach (var prop in parameters)
                {
                    builder.Append($"{prop.Type} {prop.Name}");
                    if (prop != parameters.Last())
                        builder.Append(", ");
                }
            }
            builder.Append(" ) ");
            builder.AddBracket();

            if (bodyLine != null)
                foreach (var line in bodyLine)
                {
                    builder.AppendLine(line);
                }

            builder.CloseBracket();
            builder.AppendLine();

            return builder;
        }

        public static StringBuilder AddRegion(this StringBuilder builder, string region)
        {
            builder.AppendLine($"#region [ {region} ]");
            Regions.Enqueue(region);

            return builder;
        }

        public static StringBuilder CloseRegion(this StringBuilder builder, string region)
        {
            builder.AppendLine($"#endregion [ {region} ]");
            Regions.ToList().Remove(region);

            return builder;
        }

        public static StringBuilder CloseAllRegion(this StringBuilder builder)
        {
            while(Regions.Any())
                builder.AppendLine($"#endregion [ {Regions.Dequeue()} ]");

            return builder;
        }

        public static string Build( this StringBuilder builder )
        {
            Properties.Clear();
            ClassName = "";
            BracketsToClose = 0;
            return builder.ToString();
        }

        public static StringBuilder AddBracket( this StringBuilder builder ) {
            builder.AppendLine( "{" );
            BracketsToClose++;
            return builder;
        }

        public static StringBuilder CloseBracket( this StringBuilder builder ) {
            builder.AppendLine( "}" );
            BracketsToClose--;
            return builder;
        }

        public static StringBuilder CloseAllBrackets( this StringBuilder builder ) {
            var brackets = BracketsToClose;
            for ( int i = 0; i < brackets; i++ ) {
                builder.CloseBracket( );
            }
            return builder;
        }

        private static StringBuilder AddModifier( this StringBuilder builder, IEnumerable<Modifier> modifiers ) {
            if (modifiers != null)
            {
                if (modifiers.ToList().Contains(Modifier.PRIVATE))
                    builder.Append("private ");
                else if (modifiers.ToList().Contains(Modifier.PROTECTED))
                    builder.Append("protected ");
                else if (modifiers.ToList().Contains(Modifier.PUBLIC))
                    builder.Append("public ");

                if (modifiers.ToList().Contains(Modifier.STATIC))
                    builder.Append("static ");

                if (modifiers.ToList().Contains(Modifier.OVERRIDE))
                    builder.Append("override ");

                if (modifiers.ToList().Contains(Modifier.ASYNC))
                    builder.Append("async ");

                if (modifiers.ToList().Contains(Modifier.VIRTUAL))
                    builder.Append("virtual ");

                if (modifiers.ToList().Contains(Modifier.READONLY))
                    builder.Append("readonly ");
            }
            return builder;
        }

        private static string ModifierToString( Modifier modifier ) {
            switch ( modifier ) {
                case Modifier.PUBLIC: return "public";
                case Modifier.PRIVATE: return "private";
                case Modifier.PROTECTED: return "protected";
                case Modifier.STATIC: return "static";
                case Modifier.OVERRIDE: return "override";
                case Modifier.ASYNC: return "async";
                case Modifier.VIRTUAL: return "virtual";
                case Modifier.READONLY: return "readonly";
                default: return "";
            }
        }

        public static StringBuilder AddInterface( this StringBuilder builder, string interfaceName, IEnumerable<Modifier> modifiers = null, IEnumerable<string> implementsClass = null)
        {
            builder.AddModifier(modifiers);

            builder.Append($"interface { interfaceName }");

            if (implementsClass != null)            {
                builder.Append(" : ");                              
                    foreach (var imp in implementsClass)
                    {
                        builder.Append(imp);
                        if (imp != implementsClass.Last())
                        {
                            builder.Append(", ");
                        }
                    }                
            }

            builder.AddBracket();
            builder.AppendLine();

            ClassName = interfaceName;

            return builder;
        }

        public static StringBuilder AddMethodDeclaration(this StringBuilder builder, string methodName, string returnType, IEnumerable<Property> parameters = null)
        {
            builder.Append($"{returnType} {methodName} ( ");
            foreach (var prop in parameters)
            {
                builder.Append($"{prop.Type} {prop.Name}");
                if (prop != parameters.Last())
                    builder.Append(", ");
            }
            builder.AppendLine(" ); ");
            return builder;
        }
    }
}
