﻿using System.Collections.Generic;

namespace CodeGenerator.Generators.Domain
{
    public class ConstantGenerator : Generator
    {
        public ConstantGenerator(string className, string type) : base(className)
        {
            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass(className, new List<Modifier> { Modifier.PUBLIC }, $"Constant<{type}>")
                .AddConstructor(false, true, new List<Property> { new Property("value", type, false, true) })
                .AddRegion($"{className}Constants")
                .CloseRegion($"{className}Constants")
                .CloseAllBrackets();
        }
    }
}
