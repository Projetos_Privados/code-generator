﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Models
{
    public class AttributeParam
    {
        private string Name { get; set; }
        private List<string> Values { get; set; }

        public AttributeParam(string name, params string[] value)
        {
            Name = name;
            Values = new List<string>(value);
        }

        public override string ToString()
        {
            return $"[{Name}({String.Join(", ", Values.ToArray())})]";
        }
    }
}
