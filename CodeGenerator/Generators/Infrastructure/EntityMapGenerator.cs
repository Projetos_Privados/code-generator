﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Infrastructure
{
    public class ContextMapGenerator : Generator
    {
        public ContextMapGenerator(string entityName) : base($"{entityName}Map")
        {
            _builder
                .AddUsings("Microsoft.EntityFrameworkCore" )
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{entityName}Map", new List<Modifier> { Modifier.PUBLIC }, implementsClass: new List<string> { $"IEntityTypeConfiguration<{entityName}>" })
                .AddMethod("Configure", "void", new List<Modifier> { Modifier.PUBLIC }, new List<Property> { new Property("builder", $"EntityTypeBuilder<{entityName}>") }, new List<string> { "// map properties" })
                .CloseAllBrackets();
        }
    }
}
