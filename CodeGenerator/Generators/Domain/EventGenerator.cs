﻿using System.Collections.Generic;

namespace CodeGenerator.Generators.Domain
{
    public class EventGenerator : Generator
    {
        public EventGenerator(string eventName, IEnumerable<Property> properties) : base($"{eventName}Event")
        {
            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{eventName}Event", new List<Modifier> { Modifier.PUBLIC }, "Event");

            foreach (var prop in properties)
            {
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PUBLIC }, setModifier: Modifier.PRIVATE);
            }

            _builder
                .AddConstructor(true)
                .CloseAllBrackets();
        }
    }
}
