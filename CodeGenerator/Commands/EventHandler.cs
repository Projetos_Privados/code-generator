﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("event-handler", "eh")]
    public class EventHandler : CommandBase
    {
        [Required]
        [Argument(0, Description = "Name of the command")]
        public string Command { get; set; }

        [Option( Description = "name:[type] | Repository used in the command")]
        public IEnumerable<string> Repositories { get; set; }

        [Option(Description = "name:[type] | Query used in the command")]
        public IEnumerable<string> Queries { get; set; }

        public void OnExecute()
        {
            new EventHandlerGenerator(
                    Command, 
                    Repositories.ToProperties().ToList(),
                    Queries.ToProperties().ToList()
                ).SaveToFile(Force);
        }
    }
}
