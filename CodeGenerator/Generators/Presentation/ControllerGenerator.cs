﻿using CodeGenerator.Extension;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Presentation
{
    public class ControllerGenerator : Generator
    {
        public ControllerGenerator(string controllerName, string domain, List<Property> queries) : base($"{controllerName}sController")
        {
            _builder
                .AddUsings( "MediatR", "Microsoft.AspNetCore.Http", "Microsoft.AspNetCore.Mvc", "NSwag.Annotations" )
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{controllerName}sController", new List<Modifier> { Modifier.PUBLIC }, "ApiController", attributes: new List<AttributeParam> { new AttributeParam("Route", $"\"api/{domain}/\""), new AttributeParam("OpenApiTags", $"\"{domain} / {controllerName}\"") });

            foreach(var prop in queries)
            {
                prop.SetAsQuery();
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY }, Modifier.NONE, Modifier.NONE);
            }

            _builder.AddConstructor(true, true,
                new List<Property> {
                    new Property("notifications", "INotificationHandler<DomainNotification>", true, true),
                    new Property("mediator", "IMediatorHandler", true, true),
                    new Property("mapper", "IMapper", true, true),
                    new Property("serviceProvider", "IServiceProvider", true, true)
                });

            _builder.AddMethod("Get", "Task<IActionResult>", new List<Modifier> { Modifier.PUBLIC, Modifier.ASYNC }, new List<Property> { new Property("id", "long"), Properties.CancellationToken() },
                new List<string>
                {
                    $"var response = await _{controllerName.ToCamelCase()}Query.GetAsync(id, cancellationToken);",
                    $"var result = _mapper.Map<{controllerName}ViewModel>(response);",
                    "return Response(result);"
                },
                new List<AttributeParam>
                {
                    AttributesParams.HttpGetId(),
                    new AttributeParam("OpenApiOperation", $"\"Get the {controllerName} by id\"", $"\"Return a single {controllerName}\""),
                    new AttributeParam("ProducesResponseType", $"typeof({controllerName}ViewModel)", "StatusCodes.Status200OK"),
                    new AttributeParam("ProducesResponseType", $"StatusCodes.Status404NotFound")
                }
                );

            _builder.AddMethod("Get", "Task<IActionResult>", new List<Modifier> { Modifier.PUBLIC, Modifier.ASYNC }, new List<Property> { Properties.CancellationToken() },
                new List<string>
                {
                    $"var response = await _{controllerName.ToCamelCase()}Query.GetAsync(cancellationToken);",
                    $"var result = _mapper.Map<IEnumerable<{controllerName}ViewModel>>(response);",
                    "return Response(result);"
                },
                new List<AttributeParam>
                {
                    AttributesParams.HttpGet(),
                    new AttributeParam("OpenApiOperation", $"\"Get all {controllerName}\"", $"\"Return all {controllerName}\""),
                    new AttributeParam("ProducesResponseType", $"typeof({controllerName}ViewModel)", "StatusCodes.Status200OK"),
                    new AttributeParam("ProducesResponseType", $"StatusCodes.Status404NotFound")
                }
            )
                .CloseAllBrackets();
        }
    }
}
