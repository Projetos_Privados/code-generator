﻿using CodeGenerator.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Domain
{
    public class QueryGenerator : Generator
    {
        public QueryGenerator(string name, List<Property> repositories) : base($"{name}Query")
        {
            if (repositories == null)
                repositories = new List<Property>();

            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{name}Query", new List<Modifier> { Modifier.PUBLIC }, implementsClass: new List<string> { $"I{name}Query" });

            foreach (var prop in repositories)
            {
                prop.SetAsRepository();
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PRIVATE, Modifier.READONLY }, getModifier: Modifier.NONE, setModifier: Modifier.NONE);
            }

            _builder
                .AddConstructor(true)
                .AddMethod("ExistsAsync", "Task<bool>", new List<Modifier> { Modifier.PUBLIC }, 
                    new List<Property> { 
                        new Property("id", "long"), 
                        new Property("cancellationToken", "CancellationToken") 
                    }, 
                    new List<string> {
                        "var spec = SpecificationBuilder<Regra>.Create()",
                        ".WithId(id);",
                        "",
                        $"return _{name.ToCamelCase()}Repository.AnyAsync(spec, cancellationToken);"
                    }
                 )
                .AddMethod("GetAsync", $"Task<List<{name}>>", new List<Modifier> { Modifier.PUBLIC } , null,
                    new List<string> {
                        "var spec = SpecificationBuilder<Regra>.Create();",
                        "",
                        $"return _{name.ToCamelCase()}Repository.SearchAsync(spec, cancellationToken);"
                    }
                 )
                .AddMethod("GetAsync", $"Task<{name}>", new List<Modifier> { Modifier.PUBLIC }, 
                    new List<Property> { 
                        new Property("id", "long"), 
                        new Property("cancellationToken", "CancellationToken") 
                    },
                    new List<string> {
                        $"return _{name.ToCamelCase()}Repository.FindAsync(cancellationToken, id);"
                    }
                 )
                .CloseAllBrackets();
        }
    }
}
