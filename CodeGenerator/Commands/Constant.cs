﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Domain;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("constant", "const")]
    public class Constant : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity")]
        public string Name { get; set; }

        [Required]
        [Argument(1, "Type of the entity")]
        public string Type { get; set; }

        public void OnExecute()
        {
            new ConstantGenerator(
                    Name.ToFirstUpper(), 
                    Type             
                ).SaveToFile(Force);
        }
    }
}
