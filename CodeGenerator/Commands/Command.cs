﻿using CodeGenerator.Extension;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("command", "c", "cmd")]
    public class Command : CommandBase
    {
        [Required]
        [Argument(0, Description = "Name for the command")]
        public string Name { get; set; }

        [Required]
        [Argument(1, Description = "The type of return")]
        public string Entity { get; set; }

        [Option(Description = "name:type:[y|n] | Simple property to add in model")]
        public IEnumerable<string> Properties { get; set; }


        public void OnExecute()
        {
            new CommandGenerator(
                    Name,
                    Entity,
                    Properties.ToProperties()
                ).SaveToFile(Force);
        }
    }
}
