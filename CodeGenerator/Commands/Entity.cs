﻿using CodeGenerator.Extension;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("entity", "e")]
    public class Entity : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity")]
        public string Name { get; set; }

        [Option( Description = "name:type:[y|n] | Simple property to add in model")]
        public IEnumerable<string> Properties { get; set; }

        [Option(Description = "name:type | Lists property to add in model")]
        public IEnumerable<string> Lists { get; set; }



        public void OnExecute()
        {
            new EntityGenerator(
                    Name, 
                    Properties.ToProperties(),
                    Lists.ToProperties()               
                ).SaveToFile(Force);
        }
    }
}
