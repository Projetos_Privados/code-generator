﻿using CodeGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Domain.Interfaces
{
    public class RepositoryInterfaceGenerator : Generator
    {

        public RepositoryInterfaceGenerator(string name) : base($"I{name}Repository")
        {
            _builder
                .AddUsings("System.Threading", "System.Threading.Tasks" )
                .AddNamespace(Util.GetNamespace())
                .AddInterface(
                    $"I{name}Repository", 
                    new List<Modifier> { 
                        Modifier.PUBLIC 
                    }, 
                    new List<string> { 
                        $"IReadWriteRepository<{name}>" 
                    }
                )
                .CloseAllBrackets()
                ;
        }
    }
}
