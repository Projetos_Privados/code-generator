﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Domain
{
    public class SpecificationGenerator : Generator
    {
        public SpecificationGenerator(string name, string type) : base($"{name}Specification")
        {
            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{name}Specification", new List<Modifier> { Modifier.PUBLIC, Modifier.STATIC })
                .AddMethod("WithId", $"ISpecification<{ type }>", new List<Modifier> { Modifier.PUBLIC, Modifier.STATIC }, new List<Property> { new Property("spec", $"this ISpecification<{type}>"), new Property("id", "long") }, new List<string> { "return spec.And(x => x.Id == id);" })
                .CloseAllBrackets();
        }
    }
}
