﻿using CodeGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Domain.Interfaces
{
    public class QueryInterfaceGenerator : Generator
    {

        public QueryInterfaceGenerator(string name) : base($"I{name}Query")
        {
            _builder
                .AddUsings("System.Threading", "System.Threading.Tasks" )
                .AddNamespace(Util.GetNamespace())
                .AddInterface($"I{name}Query", new List<Modifier> { Modifier.PUBLIC })
                .AddMethodDeclaration("ExistsAsync", "Task<bool>", new List<Property> { new Property("id", "long"), new Property("cancellationToken", "CancellationToken") })
                .AddMethodDeclaration("GetAsync", $"Task<List<{name}>>", new List<Property> { new Property("cancellationToken", "CancellationToken") })
                .AddMethodDeclaration("GetAsync", $"Task<{name}>", new List<Property> { new Property("id", "long"), new Property("cancellationToken", "CancellationToken") })
                .CloseAllBrackets()
                ;
        }
    }
}
