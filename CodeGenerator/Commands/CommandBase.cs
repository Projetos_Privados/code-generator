﻿using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Commands
{

    public class CommandBase
    {
        [Option(Description = "Force override file if exist")]
        public bool Force { get; set; }
    }
}
