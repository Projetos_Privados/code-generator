﻿using CodeGenerator.Extension;

namespace CodeGenerator
{

    public class Property
    {
        public string Name { get; private set; }
        public string Type { get; private set; }
        public bool Initialize { get; private set; }
        public bool ForBase { get; private set; }
        private string OriginalName { get; set; }

        public Property(string name, string type)
        {
            Name = name;
            Type = type;
            Initialize = true;

            OriginalName = Name;
        }

        public Property(string name, string type, bool initialize)
        {
            Name = name;
            Type = type;
            Initialize = initialize;

            OriginalName = Name;
        }

        public Property(string name, string type, bool initialize, bool forBase)
        {
            Name = name;
            Type = type;
            Initialize = initialize;
            ForBase = forBase;

            OriginalName = Name;
        }

        public void SetAsRepository()
        {
            Name = "_" + Name.ToCamelCase() + "Repository";
            Type += "Repository";
            Type = "I" + Type;
        }

        public void SetAsQuery()
        {
            Name = "_" + Name.ToCamelCase() + "Query";
            Type += "Query";
            Type = "I" + Type;
        }

        public void SetAsDBSet()
        {
            Name = Name.ToFirstUpper() + "s";
            Type = $"DbSet<{ Type }>";
            Initialize = false;
        }

        public string GetOriginalName()
        {
            return OriginalName;
        }

        public string GetAsParam()
        {
            return Name.ToCamelCase().Replace("_", "");
        }
    }

}
