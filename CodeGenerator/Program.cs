﻿using CodeGenerator.Commands;
using CodeGenerator.Generators.CrossCutting;
using CodeGenerator.Generators.Domain;
using CodeGenerator.Generators.Domain.Interfaces;
using CodeGenerator.Generators.Infrastructure;
using CodeGenerator.Generators.Presentation;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Text;
using static CodeGenerator.StringBuilderExtension;

namespace CodeGenerator {

    [Command(UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.Throw)]
    [Subcommand(

    #region [ Domain ]

        typeof(Entity), 
        typeof(Command), 
        typeof(CommandHandler), 
        typeof(CommandValidation),
        typeof(Constant), 
        typeof(Event),
        typeof(Commands.EventHandler),
        typeof(Query),
        typeof(Specification),
        typeof(QueryInterface),
        typeof(RepositoryInterface),
        typeof(DomainStructure),

    #endregion [ Domain ]

    #region [ Infrastructure ]

    typeof(Context),
    typeof(ContextMap),
    typeof(Repository),

    #endregion [ Infrastructure ]

    #region [ CrossCutting ]

    typeof(Injector),

    #endregion [ CrossCutting ]

    #region [ Presentation ]

    typeof(AutoMapper),
    typeof(Controller),
    typeof(ViewModel),

    #endregion [ Presentation ]

        typeof(Reuse)

        )]
    [HelpOption()]
    public class Program {

        public static int Main(string[] args)
        {
            try
            {
                return CommandLineApplication.Execute<Program>(args);
            }catch(Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return -1;
            }
        }

        [Option(Description = "The subject")]
        public string Subject { get; }

        private void OnExecute()
        {
            var subject = Subject ?? "world";
            Console.WriteLine($"Hello {subject}!");
        }

        //static void Main( string[ ] args ) {


        //    var aggregate = new AggregateGenerator(
        //        "Regra", 
        //        new List<Property> { new Property("Id", "long", false), new Property("Name", "string") },
        //        new List<Property> { new Property("Unidades", "RegraUnidade", false) }                
        //        ).Generate();

        //    var command = new CommandGenerator(
        //        "CriarRegra",
        //        "Regra",
        //        new List<Property> { 
        //            new Property("Id", "long", false), 
        //            new Property("Name", "string") ,
        //            new Property("Unidades", "IEnumerable<long>") 
        //        }
        //        ).Generate();

        //    var commandHandle = new CommandHandleGenerator(
        //        "CriarRegra",
        //        "Regra",
        //        new List<Property> { new Property("_regra", "Regra") },
        //        new List<Property> {
        //            new Property("_parametrizacaoAtividade", "ParametrizacaoAtividade") ,
        //            new Property("_unidade", "Unidade") ,
        //            new Property("_natureza", "Natureza") ,
        //        }
        //        ).Generate();


        //    var validation = new CommandValidationGenerator(
        //        "CriarRegra",
        //        "Regra",
        //        new List<Property> { new Property("regra", "Regra"), new Property("classificacao", "Classificacao") }
        //        ).Generate();

        //    var queryInterface = new QueryInterfaceGenerator("Regra").Generate();

        //    var repositoryInterface = new RepositoryInterfaceGenerator("Regra").Generate();

        //    var constant = new ConstantGenerator("Atividade", "int").Generate();

        //    var evento = new EventGenerator("RegraAlteradaCriada",
        //        new List<Property> {
        //            new Property("NovaRegraId", "long"),
        //            new Property("AntigaRegraId", "long")
        //        }
        //    ).Generate();

        //    var eventHandle = new EventHandlerGenerator(
        //        "RegraAlteradaCriada",
        //        new List<Property> { new Property("_regraCriterio", "RegraCriterio") },
        //        new List<Property> {
        //            new Property("_regraCriterio", "RegraCriterio") ,
        //            new Property("_regra", "Regra")
        //        }
        //        ).Generate();

        //    var specification = new SpecificationGenerator("Regra", "Regra").Generate();

        //    var query = new QueryGenerator("Regra", new List<Property> { new Property("_regra", "Regra"), new Property("_classificacao", "Classificacao") }).Generate();

        //    var injector = new InjectorGenerator().Generate();

        //    var context = new ContextGenerator("AnaliseComplexidade", new List<string> { "Atividade", "Natureza", "Ocorrencia", "Unidade" }).Generate();

        //    var map = new EntityMapGenerator("Regra").Generate();

        //    var repository = new RepositoryGenerator("Regra", "AnaliseComplexidade").Generate();

        //    var controller = new ControllerGenerator("Regra", "AnaliseComplexidade", new List<Property> { new Property("_regra", "Regra") }).Generate();

        //    var viewModel = new ViewModelGenerator("CriarRegra", new List<Property> { new Property("Descricao", "string"), new Property("Ativo", "bool"), new Property("Unidades", "IEnumerable<long>") }).Generate();

        //    var autoMapper = new AutoMapperGenerator("Regra", "RegraViewModel").Generate();

        //    Console.WriteLine( "Hello World!" );
        //}
    }
}
