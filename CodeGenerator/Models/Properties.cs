﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Models
{
    public class Properties
    {
        public static Property CancellationToken()
        {
            return new Property("cancellationToken", "CancellationToken");
        }
    }
}
