﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Presentation;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("view-model", "vm")]
    public class ViewModel : CommandBase
    {
        [Required]
        [Argument(0, "Name of the view model")]
        public string Name { get; set; }

        [Option( Description = "name:type | Simple property to add in the view model")]
        public IEnumerable<string> Properties { get; set; }

        public void OnExecute()
        {
            new ViewModelGenerator(
                    Name, 
                    Properties.ToProperties().ToList()            
                ).SaveToFile(Force);
        }
    }
}
