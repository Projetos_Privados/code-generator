﻿using CodeGenerator.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeGenerator.Generators.Infrastructure
{
    public class ContextGenerator : Generator
    {
        public ContextGenerator(string contextName, List<Property> dbSets) : base($"{contextName}Context")
        {
            if (dbSets == null)
                dbSets = new List<Property>();

            _builder
                .AddUsings("Microsoft.EntityFrameworkCore" )
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{contextName}Context", new List<Modifier> { Modifier.PUBLIC }, "BaseContext");

            for (int i = 0; i < dbSets.Count; i++)
            {
                dbSets[i].SetAsDBSet();
                _builder.AddProperty(dbSets[i], new List<Modifier> { Modifier.PUBLIC }, setModifier: Modifier.PRIVATE);
            }

            var configurations = new List<string> { "modelBuilder" };
            configurations.AddRange( dbSets.Select(x => $".ApplyConfiguration( new {x.GetOriginalName().ToFirstUpper()}Map() )") );
            configurations.Add(";");
            configurations.Add("");
            configurations.Add("base.OnModelCreating(modelBuilder);");

            _builder
                .AddConstructor(true, true, new List<Property> { new Property("options", "DbContextOptions", true, true) })
                .AddMethod("OnModelCreating", "void", 
                    new List<Modifier> {
                        Modifier.PROTECTED, 
                        Modifier.OVERRIDE 
                    }, 
                    new List<Property> { 
                        new Property("modelBuilder", "ModelBuilder") 
                    }, 
                    configurations)
                .CloseAllBrackets();
        }
    }
}
