﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Infrastructure;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("context-map", "ctx-map", "ctxm")]
    public class ContextMap : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity")]
        public string Name { get; set; }

        public void OnExecute()
        {
            new ContextMapGenerator(
                    Name             
                ).SaveToFile(Force);
        }
    }
}
