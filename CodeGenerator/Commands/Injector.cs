﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.CrossCutting;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("injector", "inj", "in", "i")]
    public class Injector : CommandBase
    {
        public void OnExecute()
        {
            new InjectorGenerator().SaveToFile(Force);
        }
    }
}
