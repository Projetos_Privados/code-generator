﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Presentation;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("controller", "ctr")]
    public class Controller : CommandBase
    {
        [Required]
        [Argument(0, "Name of the entity")]
        public string Name { get; set; }

        [Argument(1, "Name of the context")]
        public string Domain { get; set; }

        [Option( Description = "name:type:[y|n] | Simple property to add in model")]
        public IEnumerable<string> Queries { get; set; }

        public void OnExecute()
        {
            if (string.IsNullOrEmpty(Domain))
                Domain = Util.GetDomain();

            new ControllerGenerator(
                    Name, 
                    Domain,
                    Queries.ToProperties().ToList()
                ).SaveToFile(Force);
        }
    }
}
