﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Models
{
    public class AttributesParams
    {
        public static AttributeParam HttpGet() =>
            new AttributeParam("HttpGet", "\"[controller]\"" );

        public static AttributeParam HttpGetId() =>
            new AttributeParam("HttpGet", "\"[controller]/{id}\"" );
    }
}
