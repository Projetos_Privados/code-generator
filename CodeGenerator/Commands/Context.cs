﻿using CodeGenerator.Extension;
using CodeGenerator.Generators.Infrastructure;

using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("context", "ctx")]
    public class Context : CommandBase
    {
        [Required]
        [Argument(0, "Name of the context")]
        public string Name { get; set; }

        [Option( Description = "name | DbSets to add in model")]
        public IEnumerable<string> DbSets { get; set; }

        public void OnExecute()
        {
            new ContextGenerator(
                    Name, 
                    DbSets.ToProperties().ToList()
                ).SaveToFile(Force);
        }
    }
}
