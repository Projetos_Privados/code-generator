﻿using CodeGenerator.Generators.Presentation;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;

namespace CodeGenerator.Commands
{
    [HelpOption()]
    [Command("map", "mp", "m")]
    public class AutoMapper : CommandBase
    {
        [Required]
        [Argument(0, "Name of the source ")]
        public string Source { get; set; }

        [Required]
        [Argument(1, "Name of the destiny ")]
        public string Destiny { get; set; }

        public void OnExecute()
        {
            new AutoMapperGenerator(
                    Source,
                    Destiny
                ).SaveToFile(Force);
        }
    }
}
