﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeGenerator.Extension
{
    public static class StringExtension
    {
        public static string ToCamelCase(this string text)
        {
            if(text.Any())
                return Char.ToLowerInvariant(text.First()) + text.Substring(1);
            return text;
        }

        public static string ToFirstUpper(this string text)
        {
            if (text.Any())
                return Char.ToUpperInvariant(text.First()) + text.Substring(1);
            return text;
        }
    }
}
