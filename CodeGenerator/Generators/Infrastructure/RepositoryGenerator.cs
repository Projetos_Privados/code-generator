﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator.Generators.Infrastructure
{
    public class RepositoryGenerator : Generator
    {
        public RepositoryGenerator(string entityName, string context) : base($"{entityName}Repository")
        {
            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{entityName}Repository", new List<Modifier> { Modifier.PUBLIC }, $"ReadWriteRepository<{entityName}>", new List<string> { $"I{entityName}Repository" })
                .AddConstructor(true, true, new List<Property> { new Property("context", $"{context}Context", true, true) })
                .CloseAllBrackets();
        }
    }
}
