﻿using CodeGenerator.Generators;
using CodeGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerator
{
    public class CommandGenerator : Generator
    {
        public CommandGenerator(string commandName, string entity, IEnumerable<Property> simpleProperties) : base($"{commandName}Command")
        {
            if (simpleProperties == null)
                simpleProperties = new List<Property>();

            _builder
                .AddUsings()
                .AddNamespace(Util.GetNamespace())
                .AddClass($"{commandName}Command", new List<Modifier> { Modifier.PUBLIC }, $"Command<{entity}>");

            foreach (var prop in simpleProperties)
                _builder.AddProperty(prop, new List<Modifier> { Modifier.PUBLIC }, setModifier: Modifier.PRIVATE);

            _builder
                .AddConstructor(true)
                .CloseAllBrackets();
        }
    }
}
